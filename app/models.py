"""
SQL Alchemy models declaration.

Note, imported by alembic migrations logic, see `alembic/env.py`
"""

from typing import Any, cast
from xmlrpc.client import DateTime

from sqlalchemy import Column, ForeignKey, Integer, String, Boolean, DateTime
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.sql import func


Base = cast(Any, declarative_base())


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String(254), nullable=True)
    cellphone = Column(String(254), unique=True, index=True, nullable=False)
    hashed_password = Column(String(128), nullable=False)
    is_superuser = Column(Boolean(), default=False)
    # role = Column(String(50), nullable=True)
    is_active = Column(Boolean(), default=True)
    citizen_id = Column(String(254), unique=True, nullable=True)
    card_link = Column(String(254), nullable=True)
    address = Column(String(254), nullable=True)
    orders = relationship("Order", back_populates="owner")

class SellingItem(Base):
    __tablename__ = "item"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(500), index=True)
    package = Column(String(50))
    price = Column(Integer, nullable=True)
    stock = Column(Integer, default=0)
    
class Order(Base):
    __tablename__ = "order"
    id = Column(Integer, primary_key=True, index=True) 
    create_at = Column(String, index=True)
    update_at = Column(DateTime(timezone=True), onupdate=func.now(), nullable=True)
    item_id = Column(Integer, ForeignKey("item.id"))
    charge_id = Column(String, nullable=True)
    qty = Column(Integer)
    total_price = Column(Integer)
    is_paid = Column(Boolean, default=False)
    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="orders")
