import pytest
from httpx import AsyncClient
from app.models import User



pytestmark = pytest.mark.asyncio

    
async def test_get_single_item_endpoint(access_token, client:AsyncClient, test_create_item_endpoint):
    item_id = test_create_item_endpoint
    response = await client.get(f"/items/{item_id}", headers={"Authorization": f"Bearer {access_token}"})
    assert response.status_code == 200
    assert response.json()


async def test_get_item_endpoint(access_token, client: AsyncClient, default_user: User):
    get_items = await client.get(
        "/items/", headers={"Authorization": f"Bearer {access_token}"}
    )   
    assert get_items.status_code == 200
    assert get_items.json()
    
    
    
async def test_update_item_endpoint(access_token, client: AsyncClient, test_create_item_endpoint):
    title = "Pertamax"
    package = "20 liter"
    price = 10000
    id = test_create_item_endpoint

    update_item = await client.put(
        f"items/{id}",
        json={"id": id, "title": title, "package": package, "price": price},
        headers={"Authorization": f"Bearer {access_token}"}
    )

    assert update_item.status_code == 200
    update_item_json = update_item.json()
    assert update_item_json["title"] == title
    assert update_item_json["package"] == package
    assert update_item_json["price"] == price


     
