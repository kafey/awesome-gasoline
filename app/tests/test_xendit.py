from typing import Text
import pytest
import random
import json

from httpx import AsyncClient
from sqlalchemy.ext.asyncio.session import AsyncSession
from app.models import User


from datetime import datetime


pytestmark = pytest.mark.asyncio

async def test_charge_request_endpoint(access_token, client: AsyncClient):

    reference_id = "test-reference-id"
    currency = "IDR"
    amount = 1000
    checkout_method = "ONE_TIME_PAYMENT"
    channel_code = "ID_SHOPEEPAY"
    channel_properties = {
        "success_redirect_url": "https://dashboard.xendit.co/register/1",
    }
    metadata = {
        "branch_code": "tree_branch",
    }

    test_request_payload = {
            "reference_id": reference_id, 
            "currency": currency, 
            "amount": amount, 
            "checkout_method": checkout_method, 
            "channel_code": channel_code,
            "channel_properties": channel_properties,
            "metadata": metadata
            }

    response = await client.post(
        '/xendit/',
        data=json.dumps(test_request_payload),
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert response.status_code == 200
    response_json = response.json()
    # assert create_order_json["item_id"] == item_id
    # assert create_order_json["qty"] == qty
    # assert create_order_json["total_price"] == total_price
    charge_id = response_json["id"]


# async def test_charge_status_endpoint(access_token, client: AsyncClient):
    # charge_id = "ewc_bb8c3po-c3po-r2d2-c3po-r2d2c3por2d2"

    response = await client.get(f"/xendit/{charge_id}", headers={"Authorization": f"Bearer {access_token}"})
    assert response.status_code == 200
    assert response.json()
