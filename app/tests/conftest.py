import random
import asyncio
from typing import AsyncGenerator, Optional

import pytest
import xendit
from httpx import AsyncClient
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.config import settings
from app.core.security import get_password_hash
from app.main import app
from app.models import Base, User
from app.session import async_engine, async_session


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def client():
    async with AsyncClient(app=app, base_url="http://test") as client:
        yield client


@pytest.fixture(scope="session")
async def test_db_setup_sessionmaker():
    # assert if we use TEST_DB URL for 100%
    assert settings.ENVIRONMENT == "PYTEST"
    assert str(async_engine.url) == settings.TEST_SQLALCHEMY_DATABASE_URI

    # always drop and create test db tables between tests session
    async with async_engine.begin() as conn:

        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)
    return async_session


@pytest.fixture
async def session(test_db_setup_sessionmaker) -> AsyncGenerator[AsyncSession, None]:
    async with test_db_setup_sessionmaker() as session:
        yield session


@pytest.fixture
async def default_user(session: AsyncSession):
    result = await session.execute(select(User).where(User.cellphone == "0812345678"))
    user: Optional[User] = result.scalars().first()
    if user is None:
        new_user = User(
            cellphone="0812345678",
            hashed_password=get_password_hash("password"),
            full_name="fullname",
        )
        session.add(new_user)
        await session.commit()
        await session.refresh(new_user)
        return new_user
    return user

@pytest.fixture
async def access_token(default_user: User, client: AsyncClient):
    access_token = await client.post(
        "/auth/access-token",
        data={"username": default_user.cellphone,"password": "password"},
        headers={"Content-Type": "application/x-www-form-urlencoded"},
    )
    assert access_token.status_code == 200
    access_token = access_token.json()["access_token"]
    return access_token


@pytest.fixture
async def test_create_item_endpoint(access_token, client: AsyncClient):
    title = "Pertamax"
    package = "20 liter"
    price = 10000

    create_item = await client.post(
        "/items/",
        json={"title": title, "package": package, "price": price},
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert create_item.status_code == 201
    create_item_json = create_item.json()
    assert create_item_json["title"] == title
    assert create_item_json["package"] == package
    assert create_item_json["price"] == price
    assert "id" in create_item_json
    item_id = create_item_json["id"]
    return item_id

@pytest.fixture
def generate_random_id(param) -> str:
    return f'{random.randint(1,100)}'


