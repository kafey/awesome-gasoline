import pytest
from httpx import AsyncClient
from app.models import User

from app.core.security import get_password_hash


pytestmark = pytest.mark.asyncio


async def test_user_endpoint(client: AsyncClient, default_user: User):
    access_token = await client.post(
        "/auth/access-token",
        data={
            "username": "0812345678",
            "password": "password",
        },
        headers={"Content-Type": "application/x-www-form-urlencoded"},
    )
    assert access_token.status_code == 200
    access_token = access_token.json()["access_token"]

    # get all users
    get_user = await client.get(
        "/users/", headers={"Authorization": f"Bearer {access_token}"}
    )

    if default_user.is_superuser == True:
        assert get_user.status_code == 200
        assert get_user.json()
    else:
        assert get_user.status_code == 400
    
    # create new users
    cellphone = "08123456421"
    full_name = "New User"
    password = "123456"
    address = "desa kertajaya"

    create_user = await client.post(
        "/users/",
        json={"cellphone": cellphone, "full_name": full_name, "password": password, "address": address},
        # headers={"Authorization": f"Bearer {access_token}"},

    )
    assert create_user.status_code == 200
    create_user_json = create_user.json()
    assert create_user_json["cellphone"] == cellphone
    assert create_user_json["full_name"] == full_name

    # update user
    update_cellphone = "0812345678"
    update_name = "Update user"
    update_password = "xxccvv"
    update_address = "desa kertajaya"

    update_user = await client.put(
        "/users/me",
        json={"email": update_cellphone, "hashed_password": get_password_hash(update_password), "full_name": update_name, "address": address},
        headers={"Authorization": f"Bearer {access_token}"}
    )

    assert update_user.status_code == 200
    update_user_json = update_user.json()
    assert update_user_json["cellphone"] == update_cellphone
    assert update_user_json["full_name"] == update_name
    assert update_user_json["address"] == update_address