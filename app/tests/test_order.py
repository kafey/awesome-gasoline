from typing import Text
import pytest
import random

from httpx import AsyncClient
from sqlalchemy.ext.asyncio.session import AsyncSession
from app.models import User

import pytz
from datetime import datetime


pytestmark = pytest.mark.asyncio

async def test_create_order_endpoint(access_token, client: AsyncClient, test_create_item_endpoint, default_user:User):
    create_at = str(datetime.now(pytz.timezone('Asia/Jakarta')))
    item_id = test_create_item_endpoint
    qty = 2
    total_price = 360000
    user_id = default_user.id


    create_order = await client.post(
        '/orders/',
        json={"create_at": create_at, "item_id": item_id, "qty": qty, "total_price": total_price, "owner_id": user_id},
        headers={"Authorization": f"Bearer {access_token}"},
    )
    assert create_order.status_code == 201
    create_order_json = create_order.json()
    # assert create_order_json["item_id"] == item_id
    # assert create_order_json["qty"] == qty
    # assert create_order_json["total_price"] == total_price
    order_id = create_order_json["id"]



    response = await client.get(f"orders/{order_id}", headers={"Authorization": f"Bearer {access_token}"})
    assert response.status_code == 200
    assert response.json()

    new_qty = 2
    new_tp = 250000
    is_paid = True
    charge_id='ewc_bb8c3po-c3po-r2d2-c3po-r2d2c3por2d2'
    update_order = await client.put(
        f"orders/{order_id}",
        json={"qty":new_qty, "total_price": new_tp, "is_paid": is_paid, "charge_id": charge_id},
        headers={"Authorization": f"Bearer {access_token}"}
    )

    assert update_order.status_code == 200
    assert response.json()


async def test_get_all_order_endpoint(access_token, client: AsyncClient, default_user: User):

    response = await client.get(
        '/orders/',
        headers={"Authorization": f"Bearer {access_token}"}
    )

    if default_user.is_superuser == True:
        assert response.status_code == 200
        assert response.json()
    else:
        assert response.status_code == 400
    

async def test_get_order_by_user(access_token, client: AsyncClient, default_user):

    response = await client.get(f"orders/items/me", headers={"Authorization": f"Bearer {access_token}"})

    assert response.status_code == 200
    assert response.json()
    


       