from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy import update

from app import models, schemas
from app.api import deps

from datetime import datetime


router = APIRouter()

@router.post("/", response_model=schemas.Order, status_code=201)
async def create_orders(
    order_create: schemas.CreateOrder,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
) -> Any:
    """
    Create new item. Only for logged user.
    """
    new_order = models.Order(
        create_at=order_create.create_at,
        item_id=order_create.item_id,
        qty=order_create.qty, 
        total_price=order_create.total_price,
        owner_id = current_user.id
    )
    session.add(new_order)
    await session.commit()
    await session.refresh(new_order)

    return new_order

@router.get("/{id}", response_model=schemas.Order, status_code=200)
async def get_order(
    id: int,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
):
    result = await session.execute(select(models.Order).where(models.Order.id == id))
    order = result.scalars().first()
    return order


@router.get("/", response_model=List[schemas.Order])
async def get_all_orders(
    skip: int=0,
    limit: int=10,
    paid: bool=True,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
):
    if not current_user.is_superuser:
        raise HTTPException(
            status_code=400,
            detail="Not allowed",
        )    

    result = await session.execute(select(models.Order).order_by(models.Order.id).where(models.Order.is_paid==paid).limit(limit).offset(skip))
    orders = result.scalars().all()
    return orders


@router.get("/items/me", response_model=List[schemas.Order], status_code=200)
async def get_order_by_user(
    skip: int=0,
    limit: int=10,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
):
    result = await session.execute(select(models.Order).limit(limit).offset(skip).where(models.Order.owner_id == current_user.id))
    order = result.scalars().all()
    return order


@router.put("/{id}", response_model=schemas.Order)
async def update_order(
    id: int,
    new_order: schemas.UpdateOrder,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
)->Any:
    
    q = update(models.Order).where(models.Order.id==id)
    q = q.values(
        {
            "qty": new_order.qty,
            "charge_id": new_order.charge_id,
            "total_price": new_order.total_price,
            "is_paid": new_order.is_paid
    })
    q.execution_options(synchronize_session="fetch")
    await session.execute(q)
    await session.commit()
    
    result = await session.execute(select(models.Order).where(models.Order.id==id))
    order = result.scalars().first()
    return order




    # result = await session.execute(select(models.Order).where(models.Order.id==id))
    # order = result.scalars().first()
    # if not order:
    #     raise HTTPException(status_code=400,detail="not item please add new")
    # if new_order.qty is not None:
    #     order.qty = new_order.qty
    # if new_order.charge_id is not None:
    #     order.charge_id = new_order.charge_id
    # if new_order.total_price is not None:
    #     order.total_price = new_order.total_price
    # if new_order.is_paid is not None:
    #     order.is_paid = new_order.is_paid
    
    # await session.commit()

    # return order
