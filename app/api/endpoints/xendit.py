from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from app import models, schemas
from app.api import deps

from app.core.config import settings

from xendit import Xendit


router = APIRouter ()
api_key = settings.XENDIT_API_KEY
xendit_instance = Xendit(api_key=api_key)
EWallet = xendit_instance.EWallet

@router.post("/", response_model=schemas.ChargeStatusResponse)
async def create_charge(
    charge: schemas.CreateCharge,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
):

    

    try:
        ewallet_charge = EWallet.create_ewallet_charge(
            reference_id=charge.reference_id,
            currency=charge.currency,
            amount=charge.amount,
            checkout_method=charge.checkout_method,
            channel_code=charge.channel_code,
            channel_properties=charge.channel_properties,
            metadata=charge.metadata,
        )
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail=str(e))
    return {
        "id": ewallet_charge.id,
        "reference_id": ewallet_charge.reference_id,
        "status": ewallet_charge.status,
        "channel_properties": ewallet_charge.channel_properties,
        "action": ewallet_charge.actions
    }

   
    
@router.get("/{charge_id}", response_model=schemas.ChargeStatusResponse)
async def get_status_charge(
    charge_id: str,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
):  

    try:
        q = charge_id
        ewallet_charge = EWallet.get_ewallet_charge_status(
            charge_id = q
        )
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return {
        "id": ewallet_charge.id,
        "reference_id": ewallet_charge.reference_id,
        "status": ewallet_charge.status,
        "channel_properties": ewallet_charge.channel_properties,
        "action": ewallet_charge.actions
    }