from typing import Any, List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from app import models, schemas
from app.api import deps

router = APIRouter()

@router.post("/", response_model=schemas.Item, status_code=201)
async def create_items(
    item_create: schemas.CreateItem,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
) -> Any:
    """
    Create new item. Only for logged user.
    """
    new_item = models.SellingItem(
        title=item_create.title,
        package=item_create.package, 
        price=item_create.price, 
        stock=item_create.stock
    )
    session.add(new_item)
    await session.commit()
    await session.refresh(new_item)

    return new_item

@router.get("/", response_model=List[schemas.Item])
async def get_all_items(
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
):
    result = await session.execute(select(models.SellingItem).order_by(models.SellingItem.id))
    items = result.scalars().all()
    return items

@router.get("/{id}", response_model=schemas.Item)
async def get_item(
    id: int,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
):
    result = await session.execute(select(models.SellingItem).where(models.SellingItem.id == id))
    item = result.scalars().first()
    return item

@router.put("/{id}")
async def update_item(
    id: int,
    new_data: schemas.UpdateItem,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user)
)-> Any:
    result = await session.execute(select(models.SellingItem).where(models.SellingItem.id==id))
    item = result.scalars().first()
    if not item:
        raise HTTPException(status_code=400,detail="not item please add new")
    if new_data.title is not None:
        item.title = new_data.title
    if new_data.package is not None:
        item.package = new_data.package
    if new_data.price is not None:
        item.price = new_data.price
    if new_data.stock is not None:
        item.stock = new_data.stock
    
    await session.commit()

    return item
   