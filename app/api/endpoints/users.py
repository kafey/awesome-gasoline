from re import U
from fastapi import APIRouter, Depends,HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from typing import Any, List

from app import models, schemas
from app.api import deps
from app.core.security import get_password_hash

router = APIRouter()


@router.post("/", response_model=schemas.User)
async def create_user(
    user_in: schemas.UserCreate,
    session: AsyncSession= Depends(deps.get_session),
    # user_in = schemas.UserCreate,
    # current_user: models.User = Depends(deps.get_current_user)
) -> Any:

    query = await session.execute(select(models.User).where(models.User.cellphone==user_in.cellphone))
    user = query.scalars().first()

    if user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system.",
        )
    
    user_in.password = get_password_hash(user_in.password)
    new_user = models.User(
        cellphone=user_in.cellphone, 
        hashed_password=user_in.password, 
        full_name=user_in.full_name,
        address=user_in.address,
        citizen_id=user_in.citizen_id,
        card_link=user_in.card_link
    )
    session.add(new_user)
    await session.commit()
    await session.refresh(new_user)
    return new_user
    
        


@router.get("/", response_model=List[schemas.User])
async def get_all(
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user),
):
    if not current_user.is_superuser:
        raise HTTPException(
            status_code=400,
            detail="Not allowed",
        )    
    result = await session.execute(select(models.User).order_by(models.User.id))
    users = result.scalars().all()
    return users



@router.get("/{id}", response_model=schemas.User)
async def get_by_id(
    id: int,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user),
    
):
    result =  await session.execute(select(models.User).where(models.User.id == id))
    users = result.scalars().all()
    user = {}
    for item in users:
        user['id'] = item.id
        user['cellphone'] = item.cellphone
        user['is_active'] = item.is_active
        user['is_superuser'] = item.is_superuser
        user['full_name'] = item.full_name
    return user
        
    
@router.put("/me", response_model=schemas.User)
async def update_user_me(
    user_update: schemas.UserUpdate,
    session: AsyncSession = Depends(deps.get_session),
    current_user: models.User = Depends(deps.get_current_user),
):
    """
    Update current user.
    """
    if user_update.password is not None:
        current_user.hashed_password = get_password_hash(user_update.password)  # type: ignore
    if user_update.full_name is not None:
        current_user.full_name = user_update.full_name  # type: ignore
    if user_update.cellphone is not None:
        current_user.cellphone = user_update.cellphone # type: ignore # type: ignore
    if user_update.address is not None:
        current_user.address = user_update.address
    if user_update.citizen_id is not None:
        current_user.citizen_id = user_update.citizen_id
    if user_update.card_link is not None:
        current_user.card_link = user_update.card_link

    session.add(current_user)
    await session.commit()
    await session.refresh(current_user)

    return current_user


@router.get("/profil/me", response_model=schemas.User)
async def read_user_me(
    current_user: models.User = Depends(deps.get_current_user),
):
    """
    Get current user.
    """
    return current_user
