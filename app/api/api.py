from fastapi import APIRouter

from app.api.endpoints import auth, items, orders, users, orders, xendit

api_router = APIRouter()
api_router.include_router(auth.router, prefix="/auth", tags=["auth"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(items.router, prefix="/items", tags= ["items"])
api_router.include_router(orders.router, prefix="/orders",tags=["orders"])
api_router.include_router(xendit.router, prefix="/xendit",tags=["xendit"])



