from typing import Optional

from pydantic import BaseModel


class BaseItem(BaseModel):
    title : str
    package: Optional[str]
    price: Optional[int]
    stock: Optional[int]


    class Config:
	    orm_mode=True


class CreateItem(BaseItem):
    pass


class Item(BaseItem):
    id: int

class UpdateItem(BaseItem):
    title: Optional[str]