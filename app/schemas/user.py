from typing import Optional

from pydantic import BaseModel, EmailStr


class BaseUser(BaseModel):
    class Config:
        orm_mode = True


class User(BaseUser):
    id: int
    cellphone: str
    is_superuser: bool
    is_active: Optional[bool] = True
    full_name: str
    address: Optional[str]
    citizen_id: Optional[str]
    card_link: Optional[str]


class UserUpdate(BaseUser):
    cellphone: Optional[str]
    password: Optional[str]
    full_name: Optional[str]
    address: Optional[str]
    citizen_id: Optional[str]
    card_link: Optional[str]


class UserCreate(BaseUser):
    cellphone: str
    password: str
    full_name: str
    address: Optional[str]
    citizen_id: Optional[str]
    card_link: Optional[str]

