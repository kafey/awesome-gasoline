from typing import Optional, Dict, Any
from pydantic import BaseModel

class BaseXendit(BaseModel):
    class Config:
        orm_mode = True

class CreateCharge(BaseXendit):
    reference_id: str
    currency: str
    amount: int
    checkout_method: str
    channel_code: str
    channel_properties: Dict[str, str]
    metadata: Dict[str, str]
    
    class Config:
        schema_extra = {
            "example": {
                "reference_id": "order-id-123",
                "currency": "IDR",
                "amount": 25000,
                "checkout_method": "ONE_TIME_PAYMENT",
                "channel_code": "ID_LINKAJA",
                "channel_properties": {
                    "success_redirect_url": "https://redirect.me/payment"
                },
                "metadata": {
                    "branch_area": "CIKUTRA",
                    "branch_city": "BANDUN"
                }
            }
        }
    

class ChargeStatusResponse(BaseXendit):
    id: str
    reference_id: str
    status: str
    channel_properties: Dict[Any, Any]
    action: Dict[Any, Any]

class ChargeStatusRequest(BaseXendit):
    charge_id: str


