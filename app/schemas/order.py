from typing import Optional

from datetime import datetime
from pydantic import BaseModel
from pydantic.errors import IntegerError

class BaseOrder(BaseModel):
    class Config:
        orm_mode = True

class CreateOrder(BaseOrder):
    item_id: int
    create_at: str
    qty: int
    total_price: int
    is_paid: Optional[bool] = False
    owner_id: int

class UpdateOrder(BaseOrder):
    charge_id: str
    is_paid: Optional[bool]
    qty: Optional[int]
    total_price: Optional[int]

class Order(BaseOrder):
    id: int
    item_id: int
    charge_id: Optional[str]
    qty: int
    total_price: int
    is_paid: bool
    update_at: Optional[datetime]
    create_at: str
    owner_id: int
